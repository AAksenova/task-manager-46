package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.model.IRepository;
import ru.t1.aksenova.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
