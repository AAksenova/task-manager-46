package ru.t1.aksenova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

public interface IDTORepository<M extends AbstractModelDTO> {

    M add(@NotNull final M model);

    void update(@NotNull final M model);

    void remove(@NotNull final M model);

}
